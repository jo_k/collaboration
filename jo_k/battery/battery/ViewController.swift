//
//  ViewController.swift
//  battery
//
//  Created by Kei on 06/10/2019.
//  Copyright © 2019 J.J. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {

    @IBOutlet weak var mDeviceName: UILabel!
    @IBOutlet weak var mBatteryStatus: UILabel!
    @IBOutlet weak var mBatteryLavel: UILabel!
    @IBOutlet weak var mChargeStatus: UILabel!
    @IBOutlet weak var mCommunicationCount: UILabel!
    
    var _sendedMessage: Bool = false
    var _timeCount: Int = 0
    var _timeInterval: Double = 60
    var _locationManager: CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _locationManager = CLLocationManager.init()
        _locationManager.allowsBackgroundLocationUpdates = true
        _locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        _locationManager.distanceFilter = 1000
        _locationManager.delegate = self
        
        displayInitialize()
        
        timerFiring()
    }

    func displayInitialize() {
        mDeviceName.isEnabled = false
        mDeviceName.text = UIDevice.current.name
        mBatteryStatus.isEnabled = false
        mBatteryLavel.isEnabled = false
        mChargeStatus.isEnabled = false
        mCommunicationCount.isEnabled = false
    }

    func timerFiring() {
        checkLocation()
        let timer = Timer(timeInterval: _timeInterval,
                          target: self,
                          selector: #selector(getBatteryStatus),
                          userInfo: nil,
                          repeats: true)
        RunLoop.main.add(timer, forMode: .default)
        /*
        let locationTimer = Timer(timeInterval: 60,
                                  target: self,
                                  selector: #selector(checkLocation),
                                  userInfo: nil,
                                  repeats: true)
        RunLoop.main.add(locationTimer, forMode: .default)
         */
    }
    
    @objc func checkLocation() {
        print("Check Location")
        let status = CLLocationManager.authorizationStatus()
        if (status == .notDetermined) {
            print("Check Location 許可、不許可を選択してない")
            _locationManager.requestAlwaysAuthorization();
        } else if (status == .restricted) {
            print("Check Location 機能制限している")
        } else if (status == .denied) {
            print("Check Location 許可していない")
        } else if (status == .authorizedWhenInUse) {
            print("Check Location このアプリ使用中のみ許可している")
            _locationManager.startUpdatingLocation()
        } else if (status == .authorizedAlways) {
            print("Check Location 常に許可している")
            _locationManager.startUpdatingLocation()
        }
        getBatteryStatus()
    }
    
    func sendMessageToSlack(_ text: String) {
        _sendedMessage = true
        
        let icon = ":ghost:"
        let username =  "battery_test"
        let url = "https://hooks.slack.com/services/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"
        
        let params: [String: String] = [
            "text": text,
            "icon_emoji": icon,
            "username": username
        ]

        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        try! request.httpBody = JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            // main thread
            DispatchQueue.main.async {
                var errorMessage: String?
                if let error = error {
                    errorMessage = error.localizedDescription
                } else if (response as! HTTPURLResponse).statusCode / 100 != 2 {
                    errorMessage = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
                }
                if let message = errorMessage {
                    let alert = UIAlertController(title: "Message not sent", message: message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                } else {

                }
            }
        }
        task.resume()
    }
    
    @objc func getBatteryStatus() {
        print("Check Battery Status")
        _timeCount += 1
        UIDevice.current.isBatteryMonitoringEnabled = true

        let batteryLevel = UIDevice.current.batteryLevel
        mBatteryLavel.text = NSString(format: "%.1f", batteryLevel * 100) as String + "%"
        let batteryStatus: UIDevice.BatteryState = UIDevice.current.batteryState
        var battertStatusText = ""
        switch batteryStatus {
        case .charging:
            battertStatusText = "充電中(충전중)"
            mBatteryStatus.text = battertStatusText
        case .full:
            battertStatusText = "フルチャージ(완충)"
            mBatteryStatus.text = battertStatusText
        case .unplugged:
            battertStatusText = "充電中ではありません(충전중 아님)"
            mBatteryStatus.text = battertStatusText
        case .unknown:
            battertStatusText = "Unknown(언노운)"
            mBatteryStatus.text = battertStatusText
        default:
            battertStatusText = "うん？(응??)"
            mBatteryStatus.text = battertStatusText
        }
        mCommunicationCount.text = _timeCount.description + "/240"

        UIDevice.current.isBatteryMonitoringEnabled = false
        
        // 메세지 만들기
        let deviceName = UIDevice.current.name
        let deviceSystemName = UIDevice.current.systemName
        let deviceSystemVersion = UIDevice.current.systemVersion
        let deviceInfo = deviceName + " " + deviceSystemName + " " + deviceSystemVersion + "\n" + battertStatusText
        var text = ""
        
        if batteryLevel * 100 > 80 {
            text = deviceInfo + " バッテリーが80％以上です。"
            if batteryStatus != .unplugged {
                text = text + "コードを抜いてください。"
            }
        } else if batteryLevel * 100 < 20 {
            text = deviceInfo + " バッテリーが20％以下です。充電してください。"
        } else {
            text = deviceInfo + " 現在のバッテリーレベルは：" + (batteryLevel * 100).description + "%"
        }
        
        // 배터리 레벨
        if !_sendedMessage {
            if batteryLevel * 100 > 80 {
                sendMessageToSlack(text)
            } else if batteryLevel * 100 < 20 {
                sendMessageToSlack(text)
            } else {
                sendMessageToSlack(text)
            }
        }
        
        if _timeCount > 240 {
            _timeCount = 0
            _sendedMessage = false
        }
    }
}

// MARK: CLLocationManagerDelegate
extension ViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location : CLLocation = locations.last!;
        print(location)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("LocationManager Error")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("didChangeAuthorization")
        if status == .restricted {
            print("didChangeAuthorization 기능제한")
        } else if status == .denied {
            print("didChangeAuthorization 불허가")
        } else if status == .authorizedWhenInUse {
            print("didChangeAuthorization 사용중허가")
        } else if status == .authorizedAlways {
            print("didChangeAuthorization 상시허가")
        }
    }
}
